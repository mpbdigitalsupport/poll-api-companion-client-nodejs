# Poll Api Companion Client (NodeJS)

This is a companion package for Poll Management API. - https://poll.mediaprima.com.my

## The API

The API is a consolidate place to accept and process all votes and results, when necessary, it also provides data feed with GET request.

## The Companion Client(s)

The companion clients may come in different language, for different website front-end client environment. In this example, a NodeJS version is provided. This NodeJS companion clients script is built on top of simple-oauth2 by lelylan. - https://github.com/lelylan/simple-oauth2

## Pre-Requisite

First configure the `CLIENT_ID`, `CLIENT_SECRET` and `API_BASE_URL` into the .env file. This credentials can be obtained from the Poll API `client websites` section.

```bash
$ npm install
$ npm start
```

## Web Poll Usage

### Fetch Poll Info
- To fetch a poll information, make a GET request to `/web/info/:uuid` together with the required parameters.

### Pass Vote Request
- To pass a vote request to the API, make a POST request to `/web/vote` together with the required parameters.

```javascript
{
  "uuid": "<POLL_UUID>",
  "identity_type": "<IDENTITY_TYPE>",
  "answers": ["<ANSWER1>", "<ANSWER2>"]
}
```

## Battle Poll Usage

### Fetch Poll Info
- To fetch a poll information, make a GET request to `/battle/info/:uuid` together with the required parameters.

### Pass Vote Request
- To pass a vote request to the API, make a POST request to `/battle/vote` together with the required parameters.

```javascript
{
  "uuid": "<POLL_UUID>",
  "identity_type": "<IDENTITY_TYPE>",
  "answers": ["<ANSWER1>", "<ANSWER2>"]
}
```

### Token Validation Cronjob
- The token renewal will be done via a cronjob with a checker calling to the express server. Please add the following cronjob setting to your server environment

```bash
* * * * * /usr/bin/curl http://localhost:3000/auth/validate /dev/null 2>&1
```
