'use strict'

const fs = require('fs')
const logger = require('./logger')

// Oauth2 Client Credentials
const creds = {
  client: {
    id: process.env.CLIENT_ID,
    secret: process.env.CLIENT_SECRET,
  },
  auth: {
    tokenHost: process.env.API_BASE_URL,
    tokenPath: '/oauth/token',
  },
}
const oauth2 = require('simple-oauth2').create(creds)
const tokenConfig = {}

const Auth = {
  // Authenticate
  authenticate: async function(retry) {
    await oauth2.clientCredentials
      .getToken(tokenConfig)
      .then((result) => {
        const accessToken = oauth2.accessToken.create(result)
        fs.writeFileSync('token', JSON.stringify(accessToken.token), (err) => {
            if (err) logger.error(err)
        })
      })
      .catch(retry)
  },

  // Validate token wheather it should be refreshed or not
  validateToken: function() {

    let tokenObject = this.accessToken()

    // Wrap token into the accessToken wrapper
    const accessToken = oauth2.accessToken.create(tokenObject)

    // Provide a window of time before the actual expiration to refresh the token
    const EXPIRATION_WINDOW_IN_SECONDS = 300;

    const { token } = accessToken;
    const expirationTimeInSeconds = token.expires_at.getTime() / 1000;
    const expirationWindowStart = expirationTimeInSeconds - EXPIRATION_WINDOW_IN_SECONDS;

    // If the start of the window has passed, refresh the token
    const nowInSeconds = (new Date()).getTime() / 1000;
    const shouldRefresh = nowInSeconds >= expirationWindowStart;

    // Check if the token is expired. If expired, reauthenticate
    if (shouldRefresh) {
      oauth2.clientCredentials
        .getToken(tokenConfig)
        .then((result) => {
          const accessToken = oauth2.accessToken.create(result)
          fs.writeFileSync('token', JSON.stringify(accessToken.token), (err) => {
              if (err) logger.error(err)
          })
          logger.info('---> Access token renewal succesfully completed')
        })
        .catch(function(err) {
          logger.error('There seems to be an issue renewing Oauth2 access token. \n', err)
        })
    }
  },

  // Retrieve token from file
  accessToken: function() {
    const token = fs.readFileSync('token', (err, data) => {
        if (err) logger.error(err)
    })
    return JSON.parse(token)
  }

}

module.exports = Auth
