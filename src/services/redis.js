'use strict'

const logger = require('./logger')

const Redis = {
  connect: () => {
    return require('redis').createClient({
      host: process.env.REDIS_HOST,
      port: process.env.REDIS_PORT
    })
    .on('error', function (err) {
      logger.error(err)
    })
  }
}

module.exports = Redis
