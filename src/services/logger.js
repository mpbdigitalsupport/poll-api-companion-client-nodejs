'use strict'

const moment = require('moment')

const Logger = {

  info: function (message) {
    console.log('INFO [' + this.timestamp() + '] ' + message)
  },

  error: function (message) {
    console.log('ERROR [' + this.timestamp() + '] ' + message)
  },

  timestamp: function () {
    return moment().format('YYYY-MM-DD HH:mm:ss')
  }

}

module.exports = Logger
