'use strict'

const ipfilter = require('express-ipfilter').IpFilter
const IpDeniedError = require('express-ipfilter').IpDeniedError
const fs = require('fs')

const Blacklist = {
  init: (app) => {

    const ips = fs.readFileSync('blacklist.ip', (err, data, fs) => {
      if (err) {
        logger.error(err)
        fs.writeFileSync('blacklist.ip', '', (err) => {
          if (err) logger.error(err)
        })
      }
    }).toString().split("\n")

    const detectTrueClientIp = (req) => req.clientIp

    app.use(ipfilter(ips, { log: false, detectIp: detectTrueClientIp }))
    app.use(function(err, req, res, _next) {
      if (err instanceof IpDeniedError) res.sendStatus(401)
    })
  }
}

module.exports = Blacklist
