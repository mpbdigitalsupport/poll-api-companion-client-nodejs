'use strict'

const cors = require('cors')
const requestIp = require('request-ip')
const readline = require('readline')
const express = require('express')
const app = express()
const blacklist = require('./blacklist')
const rateLimit = require('express-rate-limit')
const redisStore = require('rate-limit-redis')
const logger = require('./logger')

const Server = {

  up: () => {

    // 1. Attach body-parser to express
    const bodyParser = require("body-parser")
    app.use(bodyParser.urlencoded({ extended: true }))
    app.use(bodyParser.json())

    // 2. Enable CORS
    app.use(cors())

    // 3. Tell express that server is behind a proxy
    app.set('trust proxy', true);

    // 4. Rate Limiter
    const redis = require('./redis').connect()
    const voteLimiter = new rateLimit({
      store: new redisStore({
        client: redis
      }),
      max: 60,
      delayMs: 0
    });
    app.use('/web/vote', voteLimiter)

    // 5. Use request IP middleware
    app.use(requestIp.mw())
    
    // #. Security token checker middleware
    // const validateToken = (req, res, next) => {
    //   if ( ('POST' === req.method) && (process.env.AUTH_TOKEN_ENABLED === 'true') ) {
    //     if (req.headers.__token__ !== process.env.AUTH_TOKEN) return res.sendStatus(401)
    //   }
    //   next()
    // }
    // app.use(validateToken)
    
    // #. Initiate IP blacklist middleware
    // blacklist.init(app)

    // 6. Initiate HTTP Server
    app.listen(process.env.PORT, () => {
      logger.info('---> Express server started on port ' + process.env.PORT)
    })

    // 9. Enable main path for health check
    app.get('/', (req, res) => res.sendStatus(200))

    // 10. Router setup
    const auth = require('../routes/auth')
    const web = require('../routes/web')
    const battle = require('../routes/battle')

    app.use('/auth', auth)
    app.use('/web', web)
    app.use('/battle', battle)

  }

}

module.exports = Server
