'use strict'

const request = require('request')
const fs = require('fs')
const Moment = require('moment')
const logger = require('../services/logger')

const Web = {

  // Retrieve poll information
  info: function (req, res) {

    const uuid = req.params.uuid
    const url = process.env.API_BASE_URL + '/api/polls/webs/' + uuid

    request.get(url, {
      'auth': {
        'bearer': this.accessToken()
      },
      'headers': {
        'Accept': 'application/json'
      }
    }, (error, response, body) => {

      if (error) console.log(error)

      if (response.statusCode === 200) {
        res.status(200).send(JSON.parse(body))
      }
      else if (response.statusCode === 502) {
        logger.error('Warning : Bad Gateway Error')
        res.status(502).send('Bad Gateway')
      }
      else if (response.statusCode === 503) {
        logger.error('Down for maintainance.')
        res.status(503).send('Down for maintainance.')
      }
      else {
        res.status(response.statusCode).send(body)
      }

    });

  },

  // Pass vote request to API
  vote: function (req, res) {

    let timestamp = new Moment().format('YYYY-MM-DD hh:mm:ss')

    const data = {
      "uuid": req.body.uuid,
      "identity_type": req.body.identity_type,
      "identity": req.clientIp,
      "voting_time": timestamp,
      "answers": req.body.answers,
      "extra_info": ''
    }
    const url = process.env.API_BASE_URL + '/api/draft-votes/webs'

    request.post(url, {
      'auth': {
        'bearer': this.accessToken()
      },
      'headers': {
        'Accept': 'application/json'
      },
      'form': data
    }, (error, response, body) => {

      if (error) console.log(error)

      if (response.statusCode === 200) {
        res.status(200).send(JSON.parse(body))
      }
      else if (response.statusCode === 502) {
        logger.error('Warning : Bad Gateway Error')
        res.status(502).send('Bad Gateway')
      }
      else if (response.statusCode === 503) {
        logger.error('Down for maintainance.')
        res.status(503).send('Down for maintainance.')
      }
      else {
        res.status(response.statusCode).send(body)
      }

    })

  },

  // Retrieve token
  accessToken: function () {
    const token = fs.readFileSync('token', (err, data) => {
        if (err) logger.error(err)
    })
    return JSON.parse(token).access_token
  }

};

module.exports = Web
