'use strict'

require('dotenv').config()
const auth = require('./services/authenticate')
const server = require('./services/server')
const logger = require('./services/logger')

// Promise retry setup
const promiseRetry = require('promise-retry')
const promiseRetryOptions = {
  forever: true,
  factor: 2,
  minTimeout: 3 * 1000,
  maxTimeout: 5 * 1000,
  randomize: true
}

promiseRetry(promiseRetryOptions, function(retry, number) {

  // Authenticate oAuth2 API and start express server
  logger.info('---> Retrieve Oauth2 access token attempt ' + number)
  return auth.authenticate(retry)

})
.then(function (value) {

  // Log token retrieval success.
  logger.info('---> Oauth2 access token succesfully retrieved')

  //start express server.
  server.up()

}, function (err) {

  // Token retrieval failed, throw error.
  throw new Error(err)

});
