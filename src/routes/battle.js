'use strict'

const express = require('express')
const router = express.Router()

// Load controllers
const battle = require('../controllers/battle')

// Get batlle poll info
router.post('/info', (req, res) => {
  //
})

// Pass battle vote request
router.post('/vote', (req, res) => {
  //
})

module.exports = router
