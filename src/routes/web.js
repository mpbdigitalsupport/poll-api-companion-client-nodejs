'use strict'

const express = require('express')
const router = express.Router()

// Load controllers
const web = require('../controllers/web')

// Get web poll info
router.get('/info/:uuid', (req, res) => web.info(req, res))

// Pass web vote request
router.post('/vote', (req, res) => web.vote(req, res))

module.exports = router
