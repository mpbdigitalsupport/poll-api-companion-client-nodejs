'use strict'

const express = require('express')
const router = express.Router()
const auth = require('../services/authenticate')

// URL path for CRON Oauth2 access token validation
router.get('/validate', (req, res) => auth.validateToken())

module.exports = router
